package com.mpdemo02.springboot;

import com.mpdemo02.springboot.beans.Employee;
import com.mpdemo02.springboot.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
//@RunWith(SpringRunner.class)
class SpringbootApplicationTests {

	@Autowired
    private EmployeeMapper employeeMapper;

	@Autowired
	private RedisTemplate redisTemplate;

	@Test
	void contextLoads() {
	}

	@Test
	public void testMP(){
		Employee employee = employeeMapper.selectById(1);
        System.out.println(employee);
	}

	@Test
	public void testRedis(){
		String key = "hello";
		redisTemplate.opsForValue().set("hello","zhangsan");
		String value = (String)redisTemplate.opsForValue().get(key);
		System.out.println(value);
	}

}
