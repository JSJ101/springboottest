package com.mpdemo02.springboot.beans;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
//@TableName("user,role")
public class User_Role extends User {
    private Integer id;
    private String name;
    private String loginName;
    private String password;
    private String roleName;
}
