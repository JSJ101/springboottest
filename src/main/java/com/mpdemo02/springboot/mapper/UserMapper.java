package com.mpdemo02.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mpdemo02.springboot.beans.User;
import com.mpdemo02.springboot.beans.User_Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends BaseMapper<User>{

    List<User_Role> getUserList();

    List<User_Role> getUserList(@Param(value = "name") String name);
}
