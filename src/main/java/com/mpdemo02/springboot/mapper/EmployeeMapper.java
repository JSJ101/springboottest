package com.mpdemo02.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mpdemo02.springboot.beans.Employee;

public interface EmployeeMapper extends BaseMapper<Employee>{

}
