package com.mpdemo02.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mpdemo02.springboot.beans.User;
import com.mpdemo02.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 登陆
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;
    /**
     * 跳转登陆页面
     * @return
     */
//    @RequestMapping("/")
//    public String login(){
//        return "/login";
//    }

    /**
     * 登陆验证
     * @param name  用户名
     * @param password  密码
     * @return
     */
    @RequestMapping("/userLogin")
    public String userLogin(HttpServletRequest request, String name, String password, Map<String,String> map){
        User user = userService.getOne(new QueryWrapper<User>()
                .eq("name",name)
                .eq("password",password));
        System.out.println(user);
        if(user != null && !"".equals(user)){
            HttpSession session = request.getSession();
            session.setAttribute("userName",name);
            return "redirect:main.html";
        } else{
            map.put("msg","用户名或密码错误！");
            return "/login";
        }
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.invalidate();
        System.out.println("用户退出操作！");
        return "/login";
    }
}
