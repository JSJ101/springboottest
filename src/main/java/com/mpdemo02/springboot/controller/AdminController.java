package com.mpdemo02.springboot.controller;

import com.mpdemo02.springboot.beans.Employee;
import com.mpdemo02.springboot.beans.User;
import com.mpdemo02.springboot.service.EmployeeService;
import com.mpdemo02.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    /**
     * 查询所有用户信息列表
     * @return
     */
    @RequestMapping("/userList")
    public String queryEmpList(Model model){
        List userList = userService.getUserList();
        System.out.println("查询全部用户："+userList);
        model.addAttribute("userList",userList);
        return "/user_list";
    }

    /**
     * 根据用户名模糊查找用户列表
     * @param model
     * @param name
     * @return
     */
    @RequestMapping("/queryUserByName")
    public String queryUserByName(Model model, String name){
        List userList = userService.getUserList(name);
        System.out.println("模糊查询用户："+userList);
        model.addAttribute("userList",userList);
        return "/user_list";
    }

    /**
     * 跳转添加用户页面
     * @return
     */
    @RequestMapping("/getAddUserHtml")
    public String getAddUserHtml(){
        return "/addUser";
    }

    /**
     * 添加用户
     * @param user
     * @return
     */
    @RequestMapping("/addUser")
    public String addUser(User user){
        boolean flag = userService.save(user);
        System.out.println("添加用户："+flag);
        return "redirect:/userList";
    }

    /**
     * 跳转更新用户页面
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/toUpdateUser")
    public String toUpdateUser(Model model, String id){
        User user = userService.getById(id);
        System.out.println("更新前用户："+user);
        model.addAttribute("user",user);
        return "/edit_user";
    }

    /**
     * 更新用户
     * @param user
     * @return
     */
    @RequestMapping("/updateUser")
    public String updateUser(User user){
        boolean flag = userService.updateById(user);
        System.out.println("更新用户："+flag);
        return "redirect:/userList";
    }

    /**
     * 删除用户
     * @param id
     * @return
     */
    @RequestMapping("/deleteUser")
    public String deleteUser(Integer id){
        boolean flag = userService.removeById(id);
        System.out.println("删除用户："+flag);
        return "redirect:/userList";
    }

    /*@RequestMapping("/register")
    public String register(Employee employee){
        return "register";
    }*/

    /*@RequestMapping("/userRegister")
    public String userRegister(Employee employee){
        Boolean flag = employeeService.save(employee);
        if(flag){
            return "list";
        }
        return "index";
    }*/

    /*@RequestMapping("/query")
    @ResponseBody
    public Employee query(Integer id){
        Employee employee = employeeService.getById(id);
        System.out.println(employee);
        return employee;
    }*/
}
