package com.mpdemo02.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mpdemo02.springboot.beans.Employee;

public interface EmployeeService extends IService<Employee> {

}
