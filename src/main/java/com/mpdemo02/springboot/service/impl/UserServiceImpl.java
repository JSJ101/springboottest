package com.mpdemo02.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mpdemo02.springboot.beans.User;
import com.mpdemo02.springboot.beans.User_Role;
import com.mpdemo02.springboot.mapper.UserMapper;
import com.mpdemo02.springboot.service.UserService;
import com.mpdemo02.springboot.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Autowired
    private UserMapper userMapper;

//    @Autowired
//    private RedisUtil redisUtil;

    public List<User_Role> getUserList(){
//        List list = (List) redisUtil.get("userList");
//        if(list != null && list.size() > 0){
//            System.out.println("查询Redis");
//            return list;
//        } else{
//            List userList = userMapper.getUserList();
//            redisUtil.set("userList",userList);
//            redisUtil.expire("userList",30);
//            System.out.println("存入Redis");
//            return userList;
//        }
        List userList = userMapper.getUserList();
        return userList;
    }


    @Override
    public List<User_Role> getUserList(String name) {
        List userList = userMapper.getUserList(name);
        return userList;
    }
}
