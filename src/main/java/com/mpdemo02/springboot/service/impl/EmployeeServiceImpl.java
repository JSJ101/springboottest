package com.mpdemo02.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mpdemo02.springboot.beans.Employee;
import com.mpdemo02.springboot.mapper.EmployeeMapper;
import com.mpdemo02.springboot.service.EmployeeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper,Employee> implements EmployeeService {

}
