package com.mpdemo02.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mpdemo02.springboot.beans.User;
import com.mpdemo02.springboot.beans.User_Role;

import java.util.List;

public interface UserService extends IService<User> {

    /**
     * 查询所有用户列表
     * @return
     */
    List<User_Role> getUserList();

    /**
     * 模糊查询用户列表
     * @return
     */
    List<User_Role> getUserList(String name);
}
