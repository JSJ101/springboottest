//common模块名   ---：common.功能名             封装及兼容整理
//event对象模块名---：eventCommon.功能名
var common={};
//[01]根据类名--$(".div") 根据id--$("#div") 根据标签名--("div")*/
common.$=function (name){
	var setName=name.charAt(0);//返回对象的第一个字符
	var getName=name.substr(1)//从对象的第二个索引开始抽取所有
	switch(setName){
	     case "#":
	     return document.getElementById(getName);//根据id找标签
	     break;

	     case ".":
	     return common.getClass(getName);//根据getclass(getName)方法找类名
	     break;

	     default:
	     return document.getElementsByTagName(name);//根据标签名找标签
	}
};
//[01]根据类名找标签兼容写法
common.getClass=function (className){
	if(document.getElementsByClassName){//IE9以上的新版浏览器支持,直接返回找类名
		return document.getElementsByClassName(className);
	}
	//ie不支持的兼容写法
	var arr1=[];//新建数组
	var all=document.getElementsByTagName("*");//找到文档中所有标签
	for(var i=0; i<all.length; i++){//循环所有标签
		var arrTagName=all[i].className.split(" ");//所有标签中第N个类名,(一个标签有多个类名,以空格分割)
		for(var j=0; j<arrTagName.length; j++){//循环第n个标签中所有的类名
			   if(arrTagName[j]==className){//其中第N个标签名==classname类名
			   	 arr1.push(all[i]);//给数组添加这个类名
			   }
		}
	}
	return arr1;
};

//[02-1]获得滚动条高度,详细兼容写法,不常用
/*common.getScoll_1=function(){
	//IE9以上新版浏览器，其他新版浏览器
	if(window.pageYOffset!=null){
		var top=window.pageYOffset;
		var left=window.pageXOffset;
		return {"top":top,"left":left};
	}else if(document.compatMode=="CSS1Compat"){
		//生明了DTD兼容写法
       // 检测是不是怪异模式的浏览器 -- 就是没有 声明<!DOCTYPE html>
		var top=document.documentElement.scrollTop;
		var left=document.documentElement.scrollLeft;
		return {"top":top,"left":left};
	}else{
		//其他浏览器,和声明DTD的兼容写法，剩下的都是怪异模式的
		var top=document.body.scrollTop;
		var left=document.body.scrollLeft;
		return {"top":top,"left":left};
	}
};*/

//[03-1]获取屏幕宽高的详细兼容写法,不常用
/*common.getClient_1=function(){
	if(window.innerWidth !=null){
		var width=window.innerWidth;
		var height=window.innerHeight;
	  return{"width":width,"height":height};
	}else if(document.compatMode=="CSS1Compat"){
		var width= document.documentelement.clientWidth;
		var height=document.documentelement.clientHeight;
		return {"width":width,"height":height};
	}else{
		var width=document.body.clientWidth;
		var height=document.body.clientHeight;
		return {"width":width,"height":height};
	}
};*/
//[02]获取--滚动条宽高--简洁兼容写法,常用
common.getScroll=function(){
	var top=window.pageYOffset||document.documentElement.scrollTop||document.body.scrollTop||0;
	var left=window.pageXOffset||document.documentElement.scrollLeft||document.body.scrollLeft||0;
    return {"top":top,"left":left};
};


//[03]获取--屏幕宽高--简洁的兼容写法.常用
common.getClient=function(){
	var windth=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth||0;
	var height=window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight||0;
	return {"windth":windth,"height":height};
};

//[04]运动框架兼容写法
common.getMove =function(obj,json,fn) {  // obj对象谁运动?   json对象{"宽":值,"高":值,"透明度":值,"层次":值} fn回调函数
    clearInterval(obj.timer);//每次运动清除对象定时器
    obj.timer = setInterval(function() {
        var flag = true;  // 用来判断是否停止定时器   一定写到遍历的外面
        for(var attr in json){//for...in在json数据里面找到所有attr属性
            //开始遍历 json
            var leader = 0;
            if(attr == "opacity"){
                leader = parseInt(common.getStyle(obj,attr)*100);//
            }else{
                leader = parseInt(common.getStyle(obj,attr)); // 数值
            }
            // 目标位置就是  属性值
            var step = ( json[attr] - leader) / 10;  // 步长  用目标位置 - 现在的位置 / 10
            step = step > 0 ? Math.ceil(step) : Math.floor(step);
            //判断透明度
            if(attr == "opacity"){
                if("opacity" in obj.style){
                    obj.style.opacity = (leader + step) /100;
                }
                else{
                    obj.style.filter = "alpha(opacity = "+(leader + step)+")";
                }
            }else if(attr == "zIndex"){
                obj.style.zIndex = json[attr];
            }else{
                obj.style[attr] = leader  + step + "px" ;
            }
            // 只要其中一个不满足条件 就不应该停止定时器  这句一定遍历里面
            if(leader != json[attr])  {
                flag =  false;
            }
        }
        // 用于判断定时器的条件
        if(flag){
            clearInterval(obj.timer); //回调函数，在定时器停止后执行
            if(fn){
                   fn.call(obj);
                //或 fn();
            }
        }
    },20)
};
//[05]获得样式宽高兼容写法
common.getStyle=function(obj,attr) {
    if(obj.currentStyle){//IE兼容
        return obj.currentStyle[attr];  // 返回传递过来的某个属性
    }else {
    	//新版:获得计算样式,第二个参数   :伪类名或false
	 	return window.getComputedStyle(obj,null)[attr];
    }
};

    // ajax(XMlHttpRequest请求--->跨域问题) 动态生成script标签  第一步:首先引进访问外域网地址192.168...
  common.getCreateJs=function (url){
              var script =document.createElement("script");
              script.type="text/javascript";
              script.src=url;
              document.getElementsByTagName("body")[0].appendChild(script);
         }




//[第二个模块：eventCommon]event对象模块:   event对象各种兼容写法
var eventCommon = {
     //调用 EventUtil.addHandler(btn1,"mouseover",f1);
    //添加事件兼容写法 Handler[处理]；增加处理函数（标签，事件名，处理函数）*/
    getAddHandler: function(obj, type, fn){
        if (obj.addEventListener){
            obj.addEventListener(type, fn, false);//新版浏览器
        } else if (obj.attachEvent){//IE8以上
            obj.attachEvent("on" + type, fn);
        } else {
            obj["on" + type] = fn;
        }
    },
    //获得鼠标左右键的值
    getButton: function(event){
        if (document.implementation.hasFeature("MouseEvents", "2.0")){
            return event.button;
        } else {
            switch(event.button){
                case 0:
                case 1:
                case 3:
                case 5:
                case 7:
                    return 0;
                case 2:
                case 6:
                    return 2;
                case 4: return 1;
            }
        }
    },
    //获取键盘编码
    getCharCode: function(event){
        if (typeof event.charCode == "number"){
            return event.charCode;
        } else {
            return event.keyCode;
        }
    },

    getClipboardText: function(event){
        var clipboardData =  (event.clipboardData || window.clipboardData);
        return clipboardData.getData("text");
    },
    //获得事件对象: IE8及以前版本：window.event
    getEvent: function(event){
        return event ? event : window.event;//三目运算：
    },
    //获取鼠标经过和移开
    getRelatedTarget: function(event){
       if (event.relatedTarget){
        return event.relatedTarget;
    } else if (event.toElement){
        return event.toElement;
    } else if (event.fromElement){
        return event.fromElement;
    } else {
        return null;
    }

    },
    //获取事件源
    getTarget: function(event){
        //获取事件源           IE8获得事件源属性
        return event.target || event.srcElement;
    },

    getWheelDelta: function(event){
        if (event.wheelDelta){
            return (client.engine.opera && client.engine.opera < 9.5 ? -event.wheelDelta : event.wheelDelta);
        } else {
            return -event.detail * 40;
        }
    },
    //阻止默认行为
    getPreventDefault: function(event){
        if (event.preventDefault){
            //新版
            event.preventDefault();
        } else {
            //IE8
            event.returnValue = false;
        }
    },

    //移除事件兼容写法，移除函数(标签,事件名,处理函数)、、
    getRemoveHandler: function(element, type, handler){
        if (element.removeEventListener){
            element.removeEventListener(type, handler, false);
        } else if (element.detachEvent){
            element.detachEvent("on" + type, handler);
        } else {
            element["on" + type] = null;
        }
    },

    getSetClipboardText: function(event, value){
        if (event.clipboardData){
            event.clipboardData.setData("text/plain", value);
        } else if (window.clipboardData){
            window.clipboardData.setData("text", value);
        }
    },
    //阻止事件冒泡
    getStopPropagation: function(event){
        //新版浏览器
        if (event.stopPropagation){
            event.stopPropagation();
        } else {
            //旧版浏览器：取消冒泡
            event.cancelBubble = true;
        }
    }

};


//待续。。。。


